.PHONY: install-deps
install-deps:
	docker run --rm -v `pwd`:/app -w /app composer install

.PHONY: test
test:
	docker run --rm -v `pwd`:/app -w /app php:7.2-alpine vendor/bin/phpunit tests/DKX/Tests

.PHONY: phpstan
phpstan:
	docker run --rm -v `pwd`:/app registry.gitlab.com/dkx/docker/phpstan analyze --no-progress -l 7 -c phpstan.neon src tests
