<?php

declare(strict_types=1);

namespace DKX\MethodInjector;

use DKX\CallableParser\CallableParser;
use DKX\CallableParser\Callables\AbstractMethodCallCallable;
use DKX\MethodInjector\Exception\MissingTypeException;
use DKX\MethodInjector\Exception\UnknownTypeException;
use DKX\MethodInjector\Exception\UnsupportedCallable;
use DKX\MethodInjector\Exception\UnsupportedTypeException;
use DKX\MethodInjector\Exception\MethodDoesNotExistsException;
use DKX\MethodInjector\Providers\FactoryProvider;
use DKX\MethodInjector\Providers\ValueProvider;
use DKX\MethodInjector\Providers\Provider;
use Roave\BetterReflection\BetterReflection;
use Roave\BetterReflection\Reflection\ReflectionClass;
use Roave\BetterReflection\Reflection\ReflectionMethod;
use Roave\BetterReflection\Reflection\ReflectionParameter;
use Roave\BetterReflection\Reflection\ReflectionType;

final class MethodInjector
{


	/** @var \DKX\MethodInjector\Providers\Provider[] */
	private $providers = [];


	/**
	 * @param string $className
	 * @param mixed $value
	 * @param mixed[] $options
	 */
	public function provideValue(string $className, $value, array $options = []): void
	{
		$this->providers[$className] = new Provider(
			$className,
			new ValueProvider($value),
			$options
		);
	}


	public function provideFactory(string $className, callable $factory, array $options = []): void
	{
		$this->providers[$className] = new Provider(
			$className,
			new FactoryProvider($factory),
			$options
		);
	}


	/**
	 * @param string $className
	 * @param string $methodName
	 * @param mixed[] $staticParameters
	 * @return mixed[]
	 */
	public function inferParametersFromMethod(string $className, string $methodName, array $staticParameters = []): array
	{
		$class = (new BetterReflection)
			->classReflector()
			->reflect($className);

		if (!$class->hasMethod($methodName)) {
			throw MethodDoesNotExistsException::create($className, $methodName);
		}

		$method = $class->getMethod($methodName);
		$skipParameters = count($staticParameters);
		$params = $this->getParameters($method, $skipParameters);

		return array_merge($staticParameters, $this->mapParameters($class, $method, $params));
	}


	/**
	 * @param callable $call
	 * @param array $staticParameters
	 * @return mixed
	 */
	public function callMethod(callable $call, array $staticParameters = [])
	{
		$callable = CallableParser::parse($call);

		if (!$callable instanceof AbstractMethodCallCallable) {
			throw UnsupportedCallable::create();
		}

		$className = $callable->getClassName();
		$methodName = $callable->getMethodName();
		$inject = $this->inferParametersFromMethod($className, $methodName, $staticParameters);

		return call_user_func_array($call, $inject);
	}


	/**
	 * @param \Roave\BetterReflection\Reflection\ReflectionMethod $method
	 * @param int $skipStaticCount
	 * @return mixed[]
	 */
	private function getParameters(ReflectionMethod $method, int $skipStaticCount): array
	{
		$params = $method->getParameters();

		if ($skipStaticCount === 0) {
			return $params;
		}

		if (count($params) <= $skipStaticCount) {
			return [];
		}

		return array_slice($params, $skipStaticCount);
	}


	/**
	 * @param \Roave\BetterReflection\Reflection\ReflectionClass $class
	 * @param \Roave\BetterReflection\Reflection\ReflectionMethod $method
	 * @param \Roave\BetterReflection\Reflection\ReflectionParameter[] $parameters
	 * @return mixed[]
	 */
	private function mapParameters(ReflectionClass $class, ReflectionMethod $method, array $parameters): array
	{
		$className = $class->getName();
		$methodName = $method->getName();
		$inject = [];

		foreach ($parameters as $parameter) {
			$type = $parameter->getType();

			if ($type === null) {
				throw MissingTypeException::create($className, $methodName, $parameter->getName(), $parameter->getPosition());
			}

			$inject[] = $this->inferValue($class, $method, $parameter, $type);
		}

		return $inject;
	}


	/**
	 * @param \Roave\BetterReflection\Reflection\ReflectionClass $class
	 * @param \Roave\BetterReflection\Reflection\ReflectionMethod $method
	 * @param \Roave\BetterReflection\Reflection\ReflectionParameter $parameter
	 * @param \Roave\BetterReflection\Reflection\ReflectionType $type
	 * @return mixed
	 */
	private function inferValue(ReflectionClass $class, ReflectionMethod $method, ReflectionParameter $parameter, ReflectionType $type)
	{
		$className = $class->getName();
		$methodName = $method->getName();
		$parameterName = $parameter->getName();
		$parameterIndex = $parameter->getPosition();
		$typeValue = (string) $type;

		if ($type->isBuiltin()) {
			throw UnsupportedTypeException::create($className, $methodName, $parameterName, $parameterIndex, $typeValue);
		}

		$ctx = new InjectionContext($class, $method, $parameter, $type);

		if (array_key_exists($typeValue, $this->providers)) {
			return $this->providers[$typeValue]->provide($ctx);
		}

		foreach ($this->providers as $provider) {
			if ($provider->injectDynamically($typeValue)) {
				return $provider->provide($ctx);
			}
		}

		throw UnknownTypeException::create($className, $methodName, $parameterName, $parameterIndex, $typeValue);
	}

}
