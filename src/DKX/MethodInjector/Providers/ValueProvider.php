<?php

declare(strict_types=1);

namespace DKX\MethodInjector\Providers;

use DKX\MethodInjector\InjectionContext;

final class ValueProvider implements ProviderInterface
{


	/** @var mixed */
	private $instance;


	/**
	 * @param mixed $instance
	 */
	public function __construct($instance)
	{
		$this->instance = $instance;
	}


	/**
	 * @param \DKX\MethodInjector\InjectionContext $ctx
	 * @return mixed
	 */
	public function provide(InjectionContext $ctx)
	{
		return $this->instance;
	}

}
