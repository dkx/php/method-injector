<?php

declare(strict_types=1);

namespace DKX\MethodInjector\Providers;

use DKX\MethodInjector\InjectionContext;

interface ProviderInterface
{


	/**
	 * @param \DKX\MethodInjector\InjectionContext $ctx
	 * @return mixed
	 */
	public function provide(InjectionContext $ctx);

}
