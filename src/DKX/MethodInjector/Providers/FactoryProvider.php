<?php

declare(strict_types=1);

namespace DKX\MethodInjector\Providers;

use DKX\MethodInjector\InjectionContext;

final class FactoryProvider implements ProviderInterface
{


	/** @var callable */
	private $factory;


	public function __construct(callable $factory)
	{
		$this->factory = $factory;
	}


	/**
	 * @param \DKX\MethodInjector\InjectionContext $ctx
	 * @return mixed
	 */
	public function provide(InjectionContext $ctx)
	{
		return call_user_func($this->factory, $ctx);
	}

}
