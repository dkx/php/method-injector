<?php

declare(strict_types=1);

namespace DKX\MethodInjector\Providers;

use DKX\MethodInjector\InjectionContext;

final class Provider
{


	public const USE_FOR_CHILD = 'useForChild';


	/** @var string */
	private $className;

	/** @var \DKX\MethodInjector\Providers\ProviderInterface */
	private $innerProvider;

	/** @var bool */
	private $useForChild;


	/**
	 * @param string $className
	 * @param \DKX\MethodInjector\Providers\ProviderInterface $innerProvider
	 * @param mixed[] $options
	 */
	public function __construct(string $className, ProviderInterface $innerProvider, array $options = [])
	{
		$this->className = $className;
		$this->innerProvider = $innerProvider;
		$this->useForChild = $options[self::USE_FOR_CHILD] ?? false;
	}


	/**
	 * @param \DKX\MethodInjector\InjectionContext $ctx
	 * @return mixed
	 */
	public function provide(InjectionContext $ctx)
	{
		return $this->innerProvider->provide($ctx);
	}


	public function injectDynamically(string $className): bool
	{
		if (!$this->useForChild) {
			return false;
		}

		return is_subclass_of($className, $this->className);
	}

}
