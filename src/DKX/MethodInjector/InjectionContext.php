<?php

declare(strict_types=1);

namespace DKX\MethodInjector;

use Roave\BetterReflection\Reflection\ReflectionClass;
use Roave\BetterReflection\Reflection\ReflectionMethod;
use Roave\BetterReflection\Reflection\ReflectionParameter;
use Roave\BetterReflection\Reflection\ReflectionType;

final class InjectionContext
{


	/** @var \Roave\BetterReflection\Reflection\ReflectionClass */
	private $class;

	/** @var \Roave\BetterReflection\Reflection\ReflectionMethod */
	private $method;

	/** @var \Roave\BetterReflection\Reflection\ReflectionParameter */
	private $parameter;

	/** @var \Roave\BetterReflection\Reflection\ReflectionType */
	private $type;


	public function __construct(
		ReflectionClass $class,
		ReflectionMethod $method,
		ReflectionParameter $parameter,
		ReflectionType $type
	) {
		$this->class = $class;
		$this->method = $method;
		$this->parameter = $parameter;
		$this->type = $type;
	}


	public function getClass(): ReflectionClass
	{
		return $this->class;
	}


	public function getMethod(): ReflectionMethod
	{
		return $this->method;
	}


	public function getParameter(): ReflectionParameter
	{
		return $this->parameter;
	}


	public function getType(): ReflectionType
	{
		return $this->type;
	}

}
