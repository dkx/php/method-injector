<?php

declare(strict_types=1);

namespace DKX\MethodInjector\Exception;

final class MethodDoesNotExistsException extends \LogicException
{


	public static function create(string $className, string $methodName): self
	{
		return new self($className. '::'. $methodName. ': method does not exists');
	}

}
