<?php

declare(strict_types=1);

namespace DKX\MethodInjector\Exception;

final class UnsupportedCallable extends \LogicException
{


	public static function create(): self
	{
		return new self('Given callable is not supported. Only method calls are allowed');
	}

}
