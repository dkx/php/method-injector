<?php

declare(strict_types=1);

namespace DKX\MethodInjector\Exception;

final class UnsupportedTypeException extends \LogicException
{


	public static function create(string $className, string $methodName, string $paramName, int $paramIndex, string $type): self
	{
		return new self($className. '::'. $methodName. ': Type "'. $type. '" in parameter "'. $paramName. '" at index '. $paramIndex. ' is not supported');
	}

}
