<?php

declare(strict_types=1);

namespace DKX\MethodInjector\Exception;

final class MissingTypeException extends \LogicException
{


	public static function create(string $className, string $methodName, string $paramName, int $paramIndex): self
	{
		return new self($className. '::'. $methodName. ': parameter "'. $paramName. '" at index '. $paramIndex. ' is missing a type');
	}

}
