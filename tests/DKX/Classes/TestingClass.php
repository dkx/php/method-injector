<?php

declare(strict_types=1);

namespace DKX\TestClasses;

class TestingClass
{


	public function noParameters(): void
	{
	}


	public function onlyStaticParameters(): void
	{
	}


	/**
	 * @param mixed $a
	 */
	public function missingType($a): void
	{
	}


	public function unsupportedType(int $a): void
	{
	}


	public function injectStdClass(\stdClass $a): void
	{
	}


	public function injectStdClassChild(StdClassChild $a): void
	{
	}


	public function injectAdvanced(int $a, \stdClass $c, StdClassChild $d): void
	{
	}


	public function injectAndReturn(int $a, \stdClass $b): array
	{
		return [$a, $b];
	}


	public static function staticInjectAndReturn(int $a, \stdClass $b): array
	{
		return [$a, $b];
	}

}
