<?php

declare(strict_types=1);

namespace DKX\TestClasses;

final class StdClassChild extends \stdClass implements \Countable
{


	public function count(): int
	{
		return 1;
	}

}
