<?php

declare(strict_types=1);

namespace DKX\Tests;

use DKX\MethodInjector\Exception\UnsupportedCallable;
use DKX\MethodInjector\InjectionContext;
use DKX\MethodInjector\MethodInjector;
use DKX\MethodInjector\Exception\MissingTypeException;
use DKX\MethodInjector\Exception\UnknownTypeException;
use DKX\MethodInjector\Exception\UnsupportedTypeException;
use DKX\MethodInjector\Exception\MethodDoesNotExistsException;
use DKX\MethodInjector\Providers\Provider;
use DKX\TestClasses\TestingClass;
use DKX\TestClasses\StdClassChild;
use PHPUnit\Framework\TestCase;

final class MethodInjectorTest extends TestCase
{


	/** @var \DKX\MethodInjector\MethodInjector */
	private $injector;


	public function setUp(): void
	{
		parent::setUp();

		$this->injector = new MethodInjector;
	}
	
	
	public function testInferParametersFromMethod_method_does_not_exists(): void
	{
		self::expectException(MethodDoesNotExistsException::class);
		self::expectExceptionMessage(TestingClass::class. '::unknownMethod: method does not exists');
		
		$this->injector->inferParametersFromMethod(TestingClass::class, 'unknownMethod');
	}


	public function testInferParametersFromMethod_no_parameters(): void
	{
		$inject = $this->injector->inferParametersFromMethod(TestingClass::class, 'noParameters');

		self::assertEquals([], $inject);
	}


	public function testInferParametersFromMethod_only_static_parameters(): void
	{
		$inject = $this->injector->inferParametersFromMethod(TestingClass::class, 'onlyStaticParameters', [1]);

		self::assertEquals([1], $inject);
	}


	public function testInferParametersFromMethod_missing_type(): void
	{
		self::expectException(MissingTypeException::class);
		self::expectExceptionMessage(TestingClass::class. '::missingType: parameter "a" at index 0 is missing a type');

		$this->injector->inferParametersFromMethod(TestingClass::class, 'missingType');
	}


	public function testInferParametersFromMethod_builtin_type(): void
	{
		self::expectException(UnsupportedTypeException::class);
		self::expectExceptionMessage(TestingClass::class. '::unsupportedType: Type "int" in parameter "a" at index 0 is not supported');

		$this->injector->inferParametersFromMethod(TestingClass::class, 'unsupportedType');
	}


	public function testInferParametersFromMethod_unknown_type(): void
	{
		self::expectException(UnknownTypeException::class);
		self::expectExceptionMessage(TestingClass::class. '::injectStdClass: Could not infer value for type "'. \stdClass::class. '" in parameter "a" at index 0');

		$this->injector->inferParametersFromMethod(TestingClass::class, 'injectStdClass');
	}


	public function testInferParametersFromMethod_provideValue(): void
	{
		$value = new \stdClass;
		$this->injector->provideValue(\stdClass::class, $value);
		$inject = $this->injector->inferParametersFromMethod(TestingClass::class, 'injectStdClass');

		self::assertEquals([$value], $inject);
	}


	public function testInferParametersFromMethod_provideValue_child(): void
	{
		$value = new class extends \stdClass {};
		$this->injector->provideValue(\stdClass::class, $value);
		$inject = $this->injector->inferParametersFromMethod(TestingClass::class, 'injectStdClass');

		self::assertEquals([$value], $inject);
	}


	public function testInferParametersFromMethod_provideFactory(): void
	{
		$value = new \stdClass;
		$this->injector->provideFactory(\stdClass::class, function(InjectionContext $ctx) use ($value) {
			return $value;
		});

		$inject = $this->injector->inferParametersFromMethod(TestingClass::class, 'injectStdClass');

		self::assertEquals([$value], $inject);
	}


	public function testInferParametersFromMethod_inject_factory_child(): void
	{
		$value = new StdClassChild;
		$this->injector->provideFactory(\stdClass::class, function() use ($value) {
			return $value;
		});

		$inject = $this->injector->inferParametersFromMethod(TestingClass::class, 'injectStdClass');

		self::assertEquals([$value], $inject);
	}


	public function testInferParametersFromMethod_inject_advanced(): void
	{
		$b = new \stdClass;
		$c = new StdClassChild;

		$this->injector->provideValue(\stdClass::class, $b);
		$this->injector->provideFactory(StdClassChild::class, function() use ($c) {
			return $c;
		});

		$inject = $this->injector->inferParametersFromMethod(TestingClass::class, 'injectAdvanced', [1]);

		self::assertEquals([
			1, $b, $c,
		], $inject);
	}


	public function testInferParametersFromMethod_useForChild(): void
	{
		$value = new StdClassChild;
		$this->injector->provideValue(\stdClass::class, $value, [
			Provider::USE_FOR_CHILD => true,
		]);

		$inject = $this->injector->inferParametersFromMethod(TestingClass::class, 'injectStdClassChild');

		self::assertEquals([$value], $inject);
	}


	public function testInferParametersFromMethod_useForChild_interface(): void
	{
		$value = new StdClassChild;
		$this->injector->provideValue(\Countable::class, $value, [
			Provider::USE_FOR_CHILD => true,
		]);

		$inject = $this->injector->inferParametersFromMethod(TestingClass::class, 'injectStdClassChild');

		self::assertEquals([$value], $inject);
	}


	public function testInferParametersFromMethod_not_useForChild(): void
	{
		self::expectException(UnknownTypeException::class);
		self::expectExceptionMessage(TestingClass::class. '::injectStdClassChild: Could not infer value for type "'. StdClassChild::class. '" in parameter "a" at index 0');

		$value = new class extends TestingClass {};
		$this->injector->provideValue(TestingClass::class, $value, [
			Provider::USE_FOR_CHILD => true,
		]);

		$this->injector->inferParametersFromMethod(TestingClass::class, 'injectStdClassChild');
	}


	public function testCallMethod_unsupportedCallable(): void
	{
		self::expectException(UnsupportedCallable::class);
		self::expectExceptionMessage('Given callable is not supported. Only method calls are allowed');

		$this->injector->callMethod('count');
	}


	public function testCallMethod(): void
	{
		$b = new \stdClass;
		$class = new TestingClass;
		$this->injector->provideValue(\stdClass::class, $b);
		$injected = $this->injector->callMethod([$class, 'staticInjectAndReturn'], [5]);

		self::assertEquals([5, $b], $injected);
	}


	public function testCallMethod_static(): void
	{
		$b = new \stdClass;
		$this->injector->provideValue(\stdClass::class, $b);
		$injected = $this->injector->callMethod([TestingClass::class, 'staticInjectAndReturn'], [5]);

		self::assertEquals([5, $b], $injected);
	}

}
